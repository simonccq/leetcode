import os

def start():
	for root, dirs, files in os.walk('./src'):
		for filename in files:
			if filename.endswith('.c'):
				command = "gcc -std=c99 -c " + root + "/" + filename
				print command
				os.system(command)

start()				