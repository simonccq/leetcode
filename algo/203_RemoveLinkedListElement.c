/**
 * https://leetcode.com/problems/remove-linked-list-elements/
 *
 * Remove all elements from a linked list of integers that have value val.
 *  Example
 *      Given: 1 --> 2 --> 6 --> 3 --> 4 --> 5 --> 6, val = 6
 *      Return: 1 --> 2 --> 3 --> 4 --> 5
 */
 
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* removeElements(struct ListNode* head, int val) {
    struct ListNode** ph = &head;
    while (*ph) {
        struct ListNode* node = *ph;
        if (node->val == val) {
            *ph = node->next;
        }
        else {
            ph = &node->next;
        }
    }
    return head;    
}