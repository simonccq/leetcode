/**
 * https://leetcode.com/problems/reverse-linked-list/
 *
 * Reverse a singly linked list
 * A linked list can be reversed either iteratively or recursively. 
 * Could you implement both?
 */

#include <stddef.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* reverseList(struct ListNode* node) {
    struct ListNode* pre = NULL;
    while (node)
    {
        struct ListNode* next = node->next;
        node->next = pre;
        pre = node;
        node = next;
    }
    return pre;
}