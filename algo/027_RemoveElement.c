/**
 *  https://leetcode.com/problems/remove-element/
 *
 *  Given an array and a value, remove all instances of that value in place and return the new length.
 *  The order of elements can be changed. It doesn't matter what you leave beyond the new length.
 */

int removeElement(int* nums, int numsSize, int val) {
    int* target = nums;
    for (int i = 0; i < numsSize; i++) {
        if (nums[i] != val) {
            *target++ = nums[i];
        }
    }
    return (int)(target - nums);
}
