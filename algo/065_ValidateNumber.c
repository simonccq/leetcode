/**
 * https://leetcode.com/problems/valid-number/
 *
 * Validate if a given string is numeric.
 *  Some examples:
 *      "0" => true
 *      " 0.1 " => true
 *      "abc" => false
 *      "1 a" => false
 *      "2e10" => true
 * Note: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one.
 */

#include <stddef.h>
 
bool isNumber(const char* s)
{
    if (s == NULL)
    {
        return false;
    }

    const char* begin = s;

    // skip whitespace
    while (*begin == ' ' || *begin == '\t')
    {
        begin++;
    }

    int num = 0;
    int dot = 0;
    int exp = 0;
    int blank = 0;
    int ch = 0;
    int pre = 0;
    while ((ch = *begin++))
    {
        switch (ch)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if (pre == ' ')
                return false;
            num++;
            break;

        case '+':
        case '-':
            if (pre && !(pre == 'e' || pre == 'E') || pre == ' ')
                return false;
            break;

        case '.':
            if (dot || exp || pre == ' ')
                return false;
            dot = 1;
            break;

        case 'e':
        case 'E':
            if (num == 0 || exp || pre == ' ')
                return false;
            exp = num;
            break;

        case ' ':
        case '\t':
            break;

        default:
            return false;
        }
        pre = ch;
    }
    
    if ((num <= exp) || (dot && num == 0))
        return false;

    return num > 0;
} 