/**
 * https://leetcode.com/problems/excel-sheet-column-title/
 *
 * Given a positive integer, return its corresponding column title as appear in an Excel sheet.
 *  For example:
 *      1 -> A
 *      2 -> B
 *      3 -> C
 *      ...
 *      26 -> Z
 *      27 -> AA
 *      28 -> AB  
 */

char* convertToTile(int n) {
    int cap = 100;
    int size = 0;
    char* buf = malloc(cap);
    while (n > 0) {
        n--;
        buf[size++] = (n % 26 + 'A');
        if (size == cap) {
            cap *= 2;
            char* newbuf = malloc(cap);
            free(buf);
            buf = newbuf;
        }
        n /= 26;
    }
    buf[size] = '\0';
    char* first = buf;
    char* last = first + size;
    for (; first != last && first != --last; ++first) {
        char tmp = *first;
        *first = *last;
        *last = tmp;
    }
    return buf; 
}