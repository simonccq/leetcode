/**
 * https://leetcode.com/problems/linked-list-cycle/
 *
 * Given a linked list, determine if it has a cycle in it.
 * Note:
 *  Can you solve it without using extra space?
 */
 
#include <stdbool.h>


struct ListNode {
    int val;
    struct ListNode *next;
};

bool hasCycle(struct ListNode *head) {
    struct ListNode* fast = head;
    struct ListNode* slow = head;
    while (slow && fast) {
        fast = fast->next;
        if (fast) {
            fast = fast->next;
        }
        else {
            return false;
        }
        slow = slow->next;
        if (fast == slow) {
            return true;
        }
    }
    return false;
}