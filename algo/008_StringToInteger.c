/**
 * https://leetcode.com/problems/string-to-integer-atoi/
 *
 * Implement atoi to convert a string to an integer.
 *  Hint: Carefully consider all possible input cases. If you want a challenge, please do not see below and ask yourself what are the possible input cases. 
 *  Notes: It is intended for this problem to be specified vaguely (ie, no given input specs). You are responsible to gather all the input requirements up front.
 */
 
#include <limits.h>
#include <string.h>
#include <stdint.h>
 
int myAtoi(const char* s) {
    if (s == NULL)
        return 0;
    int len = strlen(s);
    if (len == 0)
        return 0;
    const char* b = s;
    while (*b == ' ' || *b == '\t')
        b++;
    int minus = 0;
    if (*b == '-')
    {
        minus = 1;
        b++;
    }
    else if (*b == '+')
        b++;
    const char* e = b;
    while (*e >= '0' && *e <= '9')
        e++;
    if (e == b) 
        return 0;
    if (e - b > 10) 
        return minus ? INT_MIN : INT_MAX;

    int64_t result = 0;
    while (b < e) {
        result = (result << 3) + (result << 1) + (*b) - '0';
        b++;
    }
    if (minus)
        result = -result;
    if (result > INT_MAX)
        return INT_MAX;
    else if (result < INT_MIN)
        return INT_MIN;
    return (int)result;
}
