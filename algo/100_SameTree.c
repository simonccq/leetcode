/**
 * https://leetcode.com/problems/same-tree/
 *
 * Given two binary trees, write a function to check if they are equal or not.
 * Two binary trees are considered equal if they are structurally identical and the nodes have the same value.
 */

#include <stddef.h>
#include <stdbool.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

bool isSameTree(struct TreeNode* p, struct TreeNode* q) 
{
    if (p == NULL && q == NULL)
    {
        return true;
    }
    if (p == NULL || q == NULL)
    {
        return false;
    }
    if (p->val == q->val)
    {
        bool left_equal = isSameTree(p->left, q->left);
        return left_equal && isSameTree(p->right, q->right);
    }
    return false;    
}