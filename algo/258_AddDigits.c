// https://en.wikipedia.org/wiki/Digital_root
// fast case: return 1 + (n - 1) % 9;

int addDigits(int num) {
    while (num >= 10)
    {
        int sum = 0;
        while (num > 0) {
            sum += num % 10;
            num /= 10;
        }
        num = sum;
    }
    return num; 
}
