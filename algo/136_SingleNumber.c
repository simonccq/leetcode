/**
 * https://leetcode.com/problems/single-number/
 *
 * Given an array of integers, every element appears twice except for one. Find that single one.
 * Note:
 *  Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 */
 
int singleNumber(const int* nums, int numsSize)
{
    int result = 0;
    for (int i = 0; i < numsSize; i++){
        result ^= nums[i];
    }
    return result;
}